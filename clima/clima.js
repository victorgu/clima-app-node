const axios = require('axios');

const getClima = async(lat, lng) => {

  // axios get
  let resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${ lat }&lon=${ lng }&units=metric&appid=847fde79c50651cd74232053251f2325`);

  // Error
  if ( 'ERROR' === 'ZERO_RESULTS') {
    throw new Error(`No hay resultados para lat: ${ lat } y lng: ${ lng }`);
  }

  return resp.data.main.temp;

}


module.exports = {
  getClima
}